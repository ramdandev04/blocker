const Db = require("./models/Db");

class Blocker {
    Db;
    block = false;
    host;
    constructor() {
        this.Db = new Db('wc_blocker');
    }

    blockHost = async (req, res, next) => {
        const data = req.body
        let bh = await this.Db.readOne({ name: 'host1' }, 'host')
        bh = bh.ip
        let block = false
        await bh.forEach((ok) => {
            if (data.host.indexOf(ok) > -1) {
                block = true
            }
        })
        if (block) {
            return res.json({ code: 200, block, by: 0 })
        }
        next()
    }

    blockIp1 = async (req, res, next) => {
        const data = req.body
        let bh = await this.Db.readOne({ name: 'ip1' }, 'ip')
        bh = bh.ip
        let block = false
        if (bh.includes(data.ip1)) {
            block = true
        } else {
            bh.forEach((ok) => {
                const rgx = new RegExp(ok, 'i')
                if (data.ip1.match(rgx)) {
                    block = true
                }
            })
        }
        if (block) {
            return res.json({ code: 200, block, by: 1 })
        }
        return next()
    }

    blockIp2 = async (req, res, next) => {
        let data = req.body
        let bh = await this.Db.readOne({ name: 'ip2' }, 'ip')
        bh = bh.ip
        let block = false
        bh.forEach((ok) => {
            let regex = new RegExp(ok, 'i')
            if (data.ip1.match(regex)) {
                block = true
            }
        })
        if (block) return res.json({ code: 200, block, by: 2 })
        next()
    }

    blockUa1 = async (req, res, next) => {
        let data = req.body
        let bh = await this.Db.readOne({ name: 'ua1' }, 'ua')
        bh = bh.ip
        let block = false
        bh.forEach((ok) => {
            let myua = data.ua.toLowerCase()
            if (myua.indexOf(ok.toLowerCase()) > -1 || myua == "" || myua == " " || myua == "  " || myua == "      ") {
                block = true
            }
        })
        if (block) return res.json({ code: 200, block, by: 3 })
        next()
    }

    blockUa2 = async (req, res, next) => {
        let data = req.body
        let bh = await this.Db.readOne({ name: 'ua2' }, 'ua')
        bh = bh.ip
        let block = false
        bh.forEach((ok) => {
            let myua = data.ua
            let regex = new RegExp(ok, 'i')
            if (myua.match(regex)) {
                block = true
            }
        })
        if (block) return res.json({ code: 200, block, by: 4 })
        next()
    }

    blockIsp1 = async (req, res, next) => {
        let data = req.body
        let bh = await this.Db.readOne({ name: 'isp1' }, 'isp')
        bh = bh.ip
        let block = false
        bh.forEach((ok) => {
            if (data.isp.indexOf(ok) > -1) {
                block = true
            }
        })
        if (block) return res.json({ code: 200, block, by: 5 })
        res.json({ code: 200, block })
    }
}

module.exports = new Blocker()
